package fr.codevallee.langagelinotte.atelierlinotte;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.linotte.implementations.LibrairieVirtuelleSyntaxeV2;
import org.linotte.moteur.outils.Ressources;
import org.linotte.moteur.xml.Linotte;
import org.linotte.moteur.xml.analyse.multilangage.Langage;

import java.io.IOException;
import java.io.InputStream;

import fr.codevallee.langagelinotte.atelierlinotte.fragment.CahierFragment;
import fr.codevallee.langagelinotte.atelierlinotte.fragment.TableauFragment;
import fr.codevallee.langagelinotte.atelierlinotte.fragment.ToileFragment;
import fr.codevallee.langagelinotte.atelierlinotte.ihm.AndronotteIHM;
import fr.codevallee.langagelinotte.atelierlinotte.ihm.Cahier;
import fr.codevallee.langagelinotte.atelierlinotte.ihm.LinotteTask;
import fr.codevallee.langagelinotte.atelierlinotte.outils.Preference;

public class PrincipaleActivite extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ToileFragment.OnFragmentInteractionToileListener,
        CahierFragment.OnFragmentInteractionCahierListener,
        TableauFragment.OnFragmentInteractionTableauListener {

    private Linotte linotte;
    private AndronotteIHM ihm;
    private Toast toast;
    private long lastBackPressTime = 0;
    private String version;

    private TextView tableau;
    private Cahier cahier;
    private ScrollView scrollView;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale_activite);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(PrincipaleActivite.this.getScrollView(), "Execution de votre programme", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();

                StringBuilder flux = new StringBuilder(cahier.getText());
                LinotteTask mLinotteTask = new LinotteTask(PrincipaleActivite.this, linotte, tableau, flux);
                mLinotteTask.execute();

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tableau = ((TableauFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_tableau)).getTableau();
        cahier = ((CahierFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_cahier)).getCahier();
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        ihm = new AndronotteIHM(this);
        linotte = new Linotte(new LibrairieVirtuelleSyntaxeV2(), ihm, Langage.Linotte2);

        // restaurer le livre :
        Preference.chargerLivre(this, cahier);
        cahier.getTextViewUndoRedo().clearHistory();

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        tableau.setText(tableau.getText() + version);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
                toast = Toast.makeText(this, R.string.press_back_to_quit, Toast.LENGTH_LONG);
                toast.show();
                this.lastBackPressTime = System.currentTimeMillis();
            } else {
                if (toast != null) {
                    toast.cancel();
                }
                toast = Toast.makeText(this, "Bye bye !", Toast.LENGTH_LONG);
                toast.show();
                super.onBackPressed();
                this.finish();
                System.exit(0);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principale_activite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            // On sauvegarde le livre
            Preference.sauvegardeLivre(this, cahier);
            Snackbar.make(cahier, "Livre sauvegardé !", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
            return true;
        } else if (id == R.id.action_undo) {
            cahier.getTextViewUndoRedo().undo();
            return true;
        } else if (id == R.id.action_redo) {
            cahier.getTextViewUndoRedo().redo();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_documentation) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://langagelinotte.free.fr/wordpress/"));
            startActivity(browserIntent);
        } else if (id == R.id.nav_minute) {
            Intent browserIntent = new Intent(this, WebViewActivity.class);
            startActivity(browserIntent);
        } else if (id == R.id.nav_exemple1) {
            chargerExemple("exemples/bienvenue.liv");
        } else if (id == R.id.nav_exemple2) {
            chargerExemple("exemples/clienttcp.liv");
        } else if (id == R.id.nav_exemple3) {
            chargerExemple("exemples/capitale2.liv");
        } else if (id == R.id.nav_exemple4) {
            chargerExemple("exemples/calculatrice_textuelle.liv");
        } else if (id == R.id.nav_exemple5) {
            chargerExemple("exemples/memoiredeschiffres.liv");
        } else if (id == R.id.nav_exemple6) {
            chargerExemple("exemples/fibonacci_recursif_fonction.liv");
        } else if (id == R.id.nav_exemple7) {
            chargerExemple("exemples/poeme2.liv");
        } else if (id == R.id.nav_exemple8) {
            chargerExemple("exemples/syracuse.liv");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void chargerExemple(String livre) {
        try {
            InputStream is = null;
            is = getAssets().open(livre);
            StringBuilder sb = Ressources.InputStreamToStringBuilder(is);
            is.close();
            cahier.setText(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFragmentInteractionCahier(Uri uri) {

    }

    @Override
    public void onFragmentInteractionToile(Uri uri) {

    }

    @Override
    public void onFragmentInteractionTableau(Uri uri) {

    }

    public TextView getTableau() {
        return tableau;
    }

    public FloatingActionButton getFab() {
        return fab;
    }

    public ScrollView getScrollView() {
        return scrollView;
    }

    /**
     * Called to create a dialog to be shown.
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        return ihm.onCreateDialog(id);
    }

    /**
     * If a dialog has already been created,
     * this is called to reset the dialog
     * before showing it a 2nd time. Optional.
     */
    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        ihm.onPrepareDialog(id, dialog);
    }

}
