package fr.codevallee.langagelinotte.atelierlinotte;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashActivite extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    private ImageView myImageCodeVallee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_activite);
        myImageCodeVallee = (ImageView) findViewById(R.id.imageLogo);


        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with b timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                //myImageCodeVallee.setVisibility(View.INVISIBLE);
                Intent intent = new Intent(SplashActivite.this, PrincipaleActivite.class);
                startActivity(intent);
                finish();

            }
        }, SPLASH_TIME_OUT);

    }

    @Override
    protected void onResume() {
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fadein);
        myImageCodeVallee.startAnimation(myFadeInAnimation); //Set animation to your ImageView
        super.onResume();
    }
}
