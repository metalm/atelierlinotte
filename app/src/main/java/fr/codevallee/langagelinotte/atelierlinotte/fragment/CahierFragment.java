package fr.codevallee.langagelinotte.atelierlinotte.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.codevallee.langagelinotte.atelierlinotte.R;
import fr.codevallee.langagelinotte.atelierlinotte.ihm.Cahier;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionCahierListener} interface
 * to handle interaction events.
 * Use the {@link CahierFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CahierFragment extends Fragment {
    public static final String REDOPREF = "unforedopref";
    public static final String REDOPREF_KEY = "undoredokey";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionCahierListener mListener;

    private Cahier cahier;

    public CahierFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CahierFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CahierFragment newInstance(String param1, String param2) {
        CahierFragment fragment = new CahierFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cahier, container, false);
        cahier = view.findViewById(R.id.xcahier);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteractionCahier(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionCahierListener) {
            mListener = (OnFragmentInteractionCahierListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionCahierListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public Cahier getCahier() {
        return cahier;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        SharedPreferences sp = getActivity().getSharedPreferences(REDOPREF, Context.MODE_PRIVATE);
        cahier.getTextViewUndoRedo().storePersistentState(sp.edit(), REDOPREF_KEY);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        SharedPreferences sp = getActivity().getSharedPreferences(REDOPREF, Context.MODE_PRIVATE);
        cahier.getTextViewUndoRedo().restorePersistentState(sp, REDOPREF_KEY);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionCahierListener {
        // TODO: Update argument type and name
        void onFragmentInteractionCahier(Uri uri);
    }
}
