package fr.codevallee.langagelinotte.atelierlinotte.ihm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import org.linotte.moteur.entites.Role;
import org.linotte.moteur.exception.StopException;
import org.linotte.moteur.xml.analyse.Mathematiques;
import org.linotte.moteur.xml.api.IHM;

import java.util.concurrent.Semaphore;

import fr.codevallee.langagelinotte.atelierlinotte.PrincipaleActivite;

/**
 * Created by ronan on 21/06/2017.
 */

public class AndronotteIHM implements IHM {

    public static final int TEXT_ID_Q = 1;
    private static final int DLG_DEMANDER = 0;
    private static final int DLG_QUESTIONNER = 1;
    private static final int TEXT_ID = 0;
    private final Semaphore dialogSemaphore = new Semaphore(0, true);
    //private final Semaphore dialogOpenSemaphore = new Semaphore(0, true);

    private EditText input;
    private EditText inputQ;
    private String message;
    private String output = "";

    private PrincipaleActivite activite;
    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            activite.showDialog(DLG_DEMANDER);
        }
    };
    final Handler handlerQ = new Handler() {
        public void handleMessage(Message msg) {
            activite.showDialog(DLG_QUESTIONNER);
        }
    };
    private TextView tableau;
    private ScrollView scrollView;

    public AndronotteIHM(PrincipaleActivite principaleActivite) {
        activite = principaleActivite;
        scrollView = principaleActivite.getScrollView();
        tableau = principaleActivite.getTableau();
        input = new EditText(principaleActivite);
        inputQ = new EditText(principaleActivite);

    }

    @Override
    public String demander(Role type, String acteur) throws StopException {

        Log.d("Demander", acteur);

        output = "";
        if (type == Role.NOMBRE)
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
        else
            input.setInputType(InputType.TYPE_CLASS_TEXT);

        handler.sendEmptyMessage(0);
        try {
            dialogSemaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (type == Role.NOMBRE)
            try {
                if (Mathematiques.isBigDecimal(output.trim()) == null) {
                    output = "0";
                }
            } catch (Exception e) {
                output = "0";
            }
        return output;
    }

    @Override
    public boolean afficher(final String afficher, Role type) throws StopException {

        Log.d("Afficher", afficher);

        activite.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (tableau.getText() == null || tableau.getText().length() == 0) {
                    tableau.setText(afficher);
                } else {
                    tableau.setText(tableau.getText() + "\n" + afficher);
                }
                sendScroll();
            }
        });

        return true;
    }

    @Override
    public boolean afficherErreur(String afficher) {
        return false;
    }

    @Override
    public boolean effacer() throws StopException {
        activite.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tableau.setText("");
            }
        });
        return true;
    }

    @Override
    public String questionne(String question, Role type, String acteur) throws StopException {
        output = "";
        message = question;
        //afficher(question, type);
        if (type == Role.NOMBRE)
            inputQ.setInputType(InputType.TYPE_CLASS_NUMBER);
        else
            inputQ.setInputType(InputType.TYPE_CLASS_TEXT);

        handlerQ.sendEmptyMessage(0);
        try {
            dialogSemaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (type == Role.NOMBRE)
            try {
                if (Mathematiques.isBigDecimal(output.trim()) == null) {
                    output = "0";
                }
            } catch (Exception e) {
                output = "0";
            }
        return output;

    }


    private void sendScroll() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        }).start();
    }


    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DLG_DEMANDER:
                return createInputDialog();
            case DLG_QUESTIONNER:
                return createInputDialogBox(message);
            default:
                return null;
        }
    }

    /**
     * If a dialog has already been created,
     * this is called to reset the dialog
     * before showing it a 2nd time. Optional.
     */
    public void onPrepareDialog(int id, Dialog dialog) {

        EditText text;
        switch (id) {
            case DLG_DEMANDER:
                text = (EditText) dialog.findViewById(TEXT_ID);
                text.setText("");
                break;
            case DLG_QUESTIONNER:
                text = (EditText) dialog.findViewById(TEXT_ID_Q);
                text.setText("");
                ((AlertDialog) dialog).setMessage(message);
                break;
        }
    }


    /**
     * Create and return an example alert dialog with an edit text box.
     */
    private Dialog createInputDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(activite);
        builder.setTitle("Dialogue");
        builder.setCancelable(false);
        builder.setMessage("Entrez la valeur");

        // Use an EditText view to get user input.
        input.setId(TEXT_ID);
        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                output = value;
                dialogSemaphore.release();
                return;
            }
        });

        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogSemaphore.release();
                return;
            }
        });
        return builder.create();
    }

    /**
     * Create and return an example alert dialog with an edit text box.
     */
    private Dialog createInputDialogBox(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activite);
        builder.setTitle("Dialogue");
        builder.setCancelable(false);
        builder.setMessage(message);

        // Use an EditText view to get user input.
        inputQ.setId(TEXT_ID_Q);
        builder.setView(inputQ);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = inputQ.getText().toString();
                output = value;
                dialogSemaphore.release();
                return;
            }
        });

        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogSemaphore.release();
                return;
            }
        });
        return builder.create();
    }
}
