package fr.codevallee.langagelinotte.atelierlinotte.ihm;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import fr.codevallee.langagelinotte.atelierlinotte.outils.TextViewUndoRedo;

/**
 * Created by ronan on 26/06/2017.
 */

public class Cahier extends AppCompatEditText {

    private Rect rect;
    private Paint paint;
    private TextViewUndoRedo mTextViewUndoRedo;

    public Cahier(Context context, AttributeSet attrs) {
        super(context, attrs);
        rect = new Rect();
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);
        paint.setTextSize(20);
        this.addTextChangedListener(new LinotteTextWatcher(this));
        mTextViewUndoRedo = new TextViewUndoRedo(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int baseline = getBaseline();
        for (int i = 0; i < getLineCount(); i++) {
            canvas.drawText("" + (i + 1), rect.left, baseline, paint);
            baseline += getLineHeight();
        }
        super.onDraw(canvas);
    }

    public TextViewUndoRedo getTextViewUndoRedo() {
        return mTextViewUndoRedo;
    }
}
