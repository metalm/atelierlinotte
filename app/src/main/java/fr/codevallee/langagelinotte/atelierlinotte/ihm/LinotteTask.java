package fr.codevallee.langagelinotte.atelierlinotte.ihm;

import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.alize.kernel.AKRuntime;
import org.linotte.moteur.exception.ErreurException;
import org.linotte.moteur.exception.LectureException;
import org.linotte.moteur.exception.Messages;
import org.linotte.moteur.exception.RetournerException;
import org.linotte.moteur.xml.Linotte;
import org.linotte.moteur.xml.alize.kernel.ContextHelper;
import org.linotte.moteur.xml.alize.parseur.Parseur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import fr.codevallee.langagelinotte.atelierlinotte.PrincipaleActivite;

public class LinotteTask extends AsyncTask<String, Integer, Boolean> {

    private Linotte linotte;
    // Référence faible à l'activité : pourquoi ??
    private WeakReference<PrincipaleActivite> mActivity = null;
    private TextView tv;
    private StringBuilder source;

    public LinotteTask(PrincipaleActivite pActivity, Linotte interprete, TextView tableau, StringBuilder source) {
        link(pActivity);
        tv = tableau;
        this.source = source;
        this.linotte = interprete;
        /*String r = "";
        Set<String> st = interprete.getGrammaire().getMots();
        for (String s : st
                ) {
            r += "|(" + s + ")";
        }
        Log.d("MOTS", r);*/
    }

    private static void lire(StringBuilder buffer, List<Integer> numerolignes) throws IOException {
        // Probablement pas optimum au niveau gestion de la mémoire
        BufferedReader br = new BufferedReader(new StringReader(buffer.toString()));
        int position = 0;
        String str;
        while ((str = br.readLine()) != null) {
            position += str.length() + 1;
            numerolignes.add(position);
        }
        br.close();
    }

    private static int retourneLaLigne(List<Integer> numeros, int position) {
        int ligne = 0;
        for (int entier : numeros) {
            ligne++;
            if (entier > position)
                break;

        }
        return ligne;
    }

    @Override
    protected void onPreExecute() {
        //Toast.makeText(mActivity.get(), "Lecture du livre", Toast.LENGTH_SHORT).show();
        mActivity.get().getFab().setEnabled(false);
        mActivity.get().getFab().setImageResource(android.R.drawable.presence_invisible);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        //Toast.makeText(mActivity.get(), "Arrêt du livre", Toast.LENGTH_SHORT).show();
        mActivity.get().getFab().setEnabled(true);
        mActivity.get().getFab().setImageResource(android.R.drawable.ic_media_play);
    }

    @SuppressWarnings("resource")
    @Override
    protected Boolean doInBackground(String... arg0) {

        try {
            AKRuntime runtime = new Parseur().parseLivre(new StringBuilder(source), linotte);
            // Etape 2 : on prépare l'environnement d'exécution :
            ContextHelper.populate(runtime.getContext(), linotte, null, null);
            // Etape 3 : on exécute le livre :
            runtime.execute();

        } catch (LectureException e) {
            List<Integer> numerolignes = new ArrayList<Integer>();
            try {
                lire(source, numerolignes);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            StringBuilder erreur = new StringBuilder();
            e.printStackTrace();
            if (e.getLivre() != null) {
                erreur.append("Livre : " + e.getLivre() + "\n");
            }
            erreur.append(Messages.retourneErreur(String.valueOf(e.getErreur())));
            if (e.getException().getToken() != null) {
                erreur.append(" : " + e.getException().getToken());
            }
            erreur.append(".\n");
            erreur.append("Ligne : " + retourneLaLigne(numerolignes, e.getPosition()));

            final String afficher = erreur.toString();
            mActivity.get().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (tv.getText() == null || tv.getText().length() == 0) {
                        tv.setText(afficher);
                    } else {
                        tv.setText(tv.getText() + "\n" + afficher);
                    }

                }
            });

            cancel(true);
        } catch (final RetournerException e) {

            mActivity.get().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String text = String.valueOf(e.getActeur().getValeur());
                        if (tv.getText() == null || tv.getText().length() == 0) {
                            tv.setText(text);
                        } else {
                            tv.setText(tv.getText() + "\n" + text);
                        }
                    } catch (ErreurException e1) {
                        e1.printStackTrace();
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            cancel(true);
        }

        return true;

    }

    @Override
    protected void onCancelled() {
        if (mActivity.get() != null) {
            Toast.makeText(mActivity.get(), "Arrêt du livre.", Toast.LENGTH_SHORT).show();
            mActivity.get().getFab().setEnabled(true);
            mActivity.get().getFab().setImageResource(android.R.drawable.ic_media_play);
        }
    }

    public void link(PrincipaleActivite pActivity) {
        mActivity = new WeakReference<PrincipaleActivite>(pActivity);
    }


}