package fr.codevallee.langagelinotte.atelierlinotte.ihm;

import android.graphics.Color;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ronan on 26/06/2017.
 */

public class LinotteTextWatcher implements TextWatcher {

    Cahier cahier;
    Matcher m;

    Pattern pattern_verbe = Pattern.compile(
            "(?<=\\b)((acteur)|(affiche)|(ajoute)|(annihile)|(appelle)|(asupprimer)|(attache)|(attends)|(avance)|(avec)|(bas)|(bibliothèque)|(boucle)|(café)|(casier)|(chaque)|(charge)|(cherche)|(collision)|(concatène)|(configure)|(constante)|(contient)|(convertis)|(copie)|(crée)|(dans)|(demande)|(depuis)|(différent)|(divise)|(drapeau)|(droite)|(dès)|(débogue)|(décharge)|(décrémente)|(déplace)|(efface)|(espèce)|(espèces)|(essaie)|(est)|(explore)|(extrais)|(fais)|(fusionne)|(gauche)|(globale)|(grand)|(greffons)|(haut)|(hérite)|(incrémente)|(insère)|(interromps)|(inverse)|(joue)|(lambda)|(les)|(mesure)|(milliseconde)|(millisecondes)|(modifie)|(moins)|(montre)|(multiplie)|(mélange)|(nombre)|(nomme)|(non)|(observe)|(ouvre)|(par)|(parcours)|(partir)|(peins)|(petit)|(photographie)|(pique)|(plus)|(position)|(pour)|(prend)|(projette)|(propose)|(provoque)|(que)|(questionne)|(rafraîchis)|(recharge)|(recule)|(retourne)|(reviens)|(réagir)|(référence)|(seconde)|(secondes)|(sinon)|(souffleurs)|(soustrais)|(stimule)|(suivant)|(sur)|(tableau)|(tant)|(temporise)|(termine)|(tests)|(texte)|(toile)|(touches)|(tourne)|(trie)|(vaut)|(vers)|(vide)|(égal)|(évalue)|(évoque)|(ôte)"
                    +
                    "|(demande)|(affiche)|(questionne)|(efface)|(sur)" +
                    "|(est)|(un)|(nombre)|(texte)|(valant)" +
                    "|(va)|(vers)|(retourne)|(reviens)|(casier)" +
                    "|(si)|(sinon)|(retourne)|(reviens)|(casier)" +
                    ")(?=\\b)", Pattern.CASE_INSENSITIVE);

    Pattern pattern_structure = Pattern.compile(
            "(?<=\\b)((début))(?=\\b)", Pattern.CASE_INSENSITIVE);

    Pattern pattern_bloc = Pattern.compile(
            "(?<=\\b)((lis)|(ferme))(?=\\b)", Pattern.CASE_INSENSITIVE);

    Pattern pattern_commentaires = Pattern.compile(
            "/\\*(?:.|[\\n\\r])*?\\*/|(?<!:)//.*|#.*");

    Pattern pattern_textes = Pattern.compile("\"(.*?)\"|'(.*?)'");

    Pattern pattern_fonction = Pattern.compile("(.*?):");

    Pattern pattern_nombres = Pattern.compile("(\\b(\\d*[.]?\\d+)\\b)");

    public LinotteTextWatcher(Cahier cahier) {
        this.cahier = cahier;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        //cahier.removeTextChangedListener(this);

        for (ForegroundColorSpan s : editable.getSpans(0, editable.length(), ForegroundColorSpan.class)) {
            editable.removeSpan(s);
        }
        appliquerCouleurs(editable, Color.rgb(127, 0, 85), pattern_verbe);
        appliquerCouleurs(editable, Color.rgb(149, 125, 71), pattern_structure);
        appliquerCouleurs(editable, Color.rgb(0, 127, 174), pattern_nombres);
        appliquerCouleurs(editable, Color.rgb(204, 0, 153), pattern_fonction);
        appliquerCouleurs(editable, Color.rgb(250, 172, 172), pattern_bloc);
        appliquerCouleurs(editable, Color.rgb(128, 128, 128), pattern_textes);
        appliquerCouleurs(editable, Color.rgb(79, 135, 107), pattern_commentaires);
        //cahier.addTextChangedListener(this);
    }

    private void appliquerCouleurs(Editable editable, int color, Pattern pattern) {
        m = pattern.matcher(editable.toString());
        while (m.find()) {
            editable.setSpan(
                    new ForegroundColorSpan(color),
                    m.start(),
                    m.end(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }
}
