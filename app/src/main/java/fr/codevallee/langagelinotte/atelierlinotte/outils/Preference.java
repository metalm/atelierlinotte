package fr.codevallee.langagelinotte.atelierlinotte.outils;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.TextView;

import fr.codevallee.langagelinotte.atelierlinotte.PrincipaleActivite;
import fr.codevallee.langagelinotte.atelierlinotte.R;

/**
 * Created by ronan on 23/06/2017.
 */

public class Preference {

    private final static String LIVRE = "LIV_0";

    public static void sauvegardeLivre(PrincipaleActivite principaleActivite, TextView cahier) {
        SharedPreferences sharedPreferences = principaleActivite.getSharedPreferences("livres", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LIVRE, cahier.getText().toString());
        editor.commit();
    }

    public static void chargerLivre(PrincipaleActivite principaleActivite, TextView cahier) {
        SharedPreferences sharedPreferences = principaleActivite.getSharedPreferences(principaleActivite.getString(R.string.DIC_LIV), Context.MODE_PRIVATE);
        String livre = sharedPreferences.getString(LIVRE, principaleActivite.getString(R.string.exemple));
        cahier.setText(livre);
    }
}