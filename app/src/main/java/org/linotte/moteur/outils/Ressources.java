/***********************************************************************
 * JCubitainer                                                         *
 * Version release date : May 5, 2004                                  *
 * Author : Mounès Ronan metalm@users.berlios.de                       *
 *                                                                     *
 *     http://jcubitainer.berlios.de/                                  *
 *                                                                     *
 * This code is released under the GNU GPL license, version 2 or       *
 * later, for educational and non-commercial purposes only.            *
 * If any part of the code is to be included in a commercial           *
 * software, please contact us first for a clearance at                *
 * metalm@users.berlios.de                                             *
 *                                                                     *
 *   This notice must remain intact in all copies of this code.        *
 *   This code is distributed WITHOUT ANY WARRANTY OF ANY KIND.        *
 *   The GNU GPL license can be found at :                             *
 *           http://www.gnu.org/copyleft/gpl.html                      *
 *                                                                     *
 ***********************************************************************/

/* History & changes **************************************************
 *                                                                     *
 ******** May 5, 2004 **************************************************
 *   - First release                                                   *
 ***********************************************************************/

package org.linotte.moteur.outils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

public class Ressources {

	private static final String CHEMIN = "/assets/";

	private static URLClassLoader urlLoader = null;

	private static final String ENTETE_INTERNET = "http://";

	private static final Ressources instance = new Ressources();

	public Ressources() {
	}

	public static URL getURL(String nom) {
		return Ressources.class.getResource(CHEMIN + nom);
	}

	public static InputStream getResource(String nom) {
		return Ressources.class.getResourceAsStream(CHEMIN + nom);
	}

	public static StringBuilder getResourceAsStringBuilder(String nom) throws IOException {
		InputStream in = getResource(nom);
		StringBuilder out = InputStreamToStringBuilder(in);
		return out;
	}

	public static StringBuilder InputStreamToStringBuilder(InputStream in) throws IOException {
		StringBuilder out = new StringBuilder();

		byte[] buffer = new byte[1000];

		try {
			while (true) {
				synchronized (buffer) {
					int amountRead = in.read(buffer);
					if (amountRead == -1) {
						break;
					}
					out.append(new String(buffer, 0, amountRead));
				}
			}
			out.append('\n');
		} finally {
			if (in != null) {
				in.close();
			}
		}
		return out;
	}

	public static File getdirectory(String dir) {
		// http://www.rgagnon.com/howto.html
		try {
			File current = new File(new Ressources().getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
			if (current.getName().toLowerCase().endsWith(".jar")) {
				// Si je suis dans un jar :
				current = current.getParentFile();
			} else if (current.getName().toLowerCase().endsWith(".exe")) {
				// Si je suis dans un jar :
				current = current.getParentFile();
			}
			File dg = new File(current, dir);
			// System.out.println("Chemin des greffons : " + dg);
			return dg;
		} catch (Exception e) {
			return null;
		}
	}

	public static InputStream getFlux(String nom_fichier) {
		return Ressources.class.getResourceAsStream(CHEMIN + nom_fichier);
	}

	public static File getLocal() {
		URL url = ClassLoader.getSystemResource("/");
		if (url != null)
			return new File(url.getPath());
		else
			return new File(".");
	}

	public static void setCheminReference(File fichier) {
		List<URL> urls = new ArrayList<URL>(1);
		if (fichier != null && fichier.getParent() != null)
			try {
				urls.add(new File(fichier.getParent()).toURI().toURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		else
			try {
				urls.add(getLocal().toURI().toURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		if (urlLoader != null)
			urlLoader = new URLClassLoader(urls.toArray(new URL[1]), urlLoader);
		else
			urlLoader = new URLClassLoader(urls.toArray(new URL[1]));
	}

	public static String construireChemin(String chemin) {
		if (urlLoader != null) {
			URL f = urlLoader.getResource(chemin);
			// System.out.println(f);
			if (f != null) {
				// System.out.println(urlLoader.getResource(chemin).getPath());
				try {
					return urlLoader.getResource(chemin).toURI().getPath();
				} catch (URISyntaxException e) {
					return chemin;
				}
			} else {
				// On essaye de construire le chemin à la "main"
				String pere = new File(chemin).getParent();
				if (pere != null) {
					File parent = new File(pere);
					if (parent.isAbsolute()) {
						return chemin;
					}
				}
				try {
					return urlLoader.getURLs()[0].toURI().getPath() + File.separator + chemin;
				} catch (URISyntaxException e) {
					return chemin;
				}
			}
		} else {
			return chemin;
		}
	}

	public boolean isInternetUrl(String url) {
		return url != null && url.toLowerCase().startsWith(ENTETE_INTERNET);
	}

	public String analyserChemin(String chemin) {
		return construireChemin(chemin);
	}

	public static Ressources getInstance() {
		return instance;
	}

	public void viderUrlLoader() {
		urlLoader = null;
	}

	/**
	 * http://www.rgagnon.com/javadetails/java-0662.html
	 * @param name
	 * @return
	 */
	public static String sanitizeFilename(String name) {
		return name.replaceAll("[:\\\\/*?|<>]", "_");
	}

}