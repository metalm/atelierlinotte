package org.linotte.moteur.xml;

import org.linotte.greffons.LinotteFacade;
import org.linotte.greffons.api.Greffon.Attribut;
import org.linotte.greffons.impl.Conduit;
import org.linotte.greffons.impl.Configuration;
import org.linotte.greffons.impl.FichierTube;
import org.linotte.greffons.impl.Horodatage;
import org.linotte.greffons.impl.PileTube;
import org.linotte.greffons.impl.Pipette;
import org.linotte.greffons.impl.PontTube;
import org.linotte.greffons.impl.Queue;
import org.linotte.greffons.impl.RepertoireTube;
import org.linotte.greffons.impl.Rss;
import org.linotte.greffons.impl.SQL;
import org.linotte.greffons.impl.TCPClient;
import org.linotte.greffons.impl.TCPServeur;
import org.linotte.moteur.entites.Prototype;
import org.linotte.moteur.xml.api.Librairie;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Cette classe initialise les espèces et les greffons internes
 *
 * @author CPC
 */
public final class Bibliotheque {

    private static final String TEXTE = "texte";
    private static final String NOMBRE = "nombre";

    private Bibliotheque() {
        throw new SecurityException();
    }

    public static Set<Prototype> genererEspecesGraphiquesEtGreffonsInternes(Librairie<?> lib, Linotte linotte) throws Exception {

        return greffonsInternesAutres();

    }

    /**
     * @throws Exception
     */
    private static Set<Prototype> greffonsInternesAutres() throws Exception {
        // ***************************
        // Ajout des autres greffons :
        // ***************************

        Set<Prototype> prototypes = new HashSet<>();

        prototypes.add(LinotteFacade.creationPrototype(null, new TCPClient(), "tcpclient"));
        prototypes.add(LinotteFacade.creationPrototype(null, new TCPServeur(), "tcpserveur"));

        LinotteFacade.creationPrototype(null, new Pipette(), "pipette");
        LinotteFacade.creationPrototype(null, new Queue(), "queue");

        LinotteFacade.creationPrototype(null, new Rss(), "rss");
        LinotteFacade.creationPrototype(null, new SQL(), "sql");
        LinotteFacade.creationPrototype(null, new RepertoireTube(), "répertoire");
        LinotteFacade.creationPrototype(null, new Horodatage(), "horodatage");
        LinotteFacade.creationPrototype(null, new FichierTube(), "fichier");
        LinotteFacade.creationPrototype(null, new PileTube(), "pile");
        LinotteFacade.creationPrototype(null, new Conduit(), "conduit");

        {
            Map<String, Attribut> attributs = new HashMap<String, Attribut>();
            attributs.put("adresse", new Attribut(TEXTE, ""));
            attributs.put("état", new Attribut(NOMBRE, "0"));
            attributs.put("port", new Attribut(NOMBRE, "8777"));
            LinotteFacade.creationPrototype(attributs, new PontTube(), "pont");
        }
        {
            Map<String, Attribut> attributs = new HashMap<String, Attribut>();
            attributs.put("domaine", new Attribut(TEXTE, ""));
            LinotteFacade.creationPrototype(attributs, new Configuration(), "configuration");
        }

        return prototypes;

    }

}