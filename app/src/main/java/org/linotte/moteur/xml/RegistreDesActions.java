/***********************************************************************
 * Linotte                                                             *
 * Version release date : September 01, 2006                           *
 * Author : Mounes Ronan ronan.mounes@amstrad.eu                       *
 *                                                                     *
 *     http://langagelinotte.free.fr                                   *
 *                                                                     *
 * This code is released under the GNU GPL license, version 2 or       *
 * later, for educational and non-commercial purposes only.            *
 * If any part of the code is to be included in a commercial           *
 * software, please contact us first for a clearance at                *
 *   ronan.mounes@amstrad.eu                                           *
 *                                                                     *
 *   This notice must remain intact in all copies of this code.        *
 *   This code is distributed WITHOUT ANY WARRANTY OF ANY KIND.        *
 *   The GNU GPL license can be found at :                             *
 *           http://www.gnu.org/copyleft/gpl.html                      *
 *                                                                     *
 ***********************************************************************/

package org.linotte.moteur.xml;

import org.linotte.moteur.xml.actions.EtatActeur;
import org.linotte.moteur.xml.actions.EtatAction;
import org.linotte.moteur.xml.actions.EtatAfficher;
import org.linotte.moteur.xml.actions.EtatAjouter;
import org.linotte.moteur.xml.actions.EtatAjouterSimple;
import org.linotte.moteur.xml.actions.EtatAller;
import org.linotte.moteur.xml.actions.EtatAnnihiler;
import org.linotte.moteur.xml.actions.EtatAppeler;
import org.linotte.moteur.xml.actions.EtatArreter;
import org.linotte.moteur.xml.actions.EtatAttacher;
import org.linotte.moteur.xml.actions.EtatAttendreMilliSeconde;
import org.linotte.moteur.xml.actions.EtatAttendreSeconde;
import org.linotte.moteur.xml.actions.EtatAvancerDe;
import org.linotte.moteur.xml.actions.EtatBoucle;
import org.linotte.moteur.xml.actions.EtatCharger;
import org.linotte.moteur.xml.actions.EtatChercher;
import org.linotte.moteur.xml.actions.EtatCompatibilite;
import org.linotte.moteur.xml.actions.EtatConcatener;
import org.linotte.moteur.xml.actions.EtatCondition;
import org.linotte.moteur.xml.actions.EtatConditionSinon;
import org.linotte.moteur.xml.actions.EtatConfigurer;
import org.linotte.moteur.xml.actions.EtatConvertir;
import org.linotte.moteur.xml.actions.EtatCopier;
import org.linotte.moteur.xml.actions.EtatDecharger;
import org.linotte.moteur.xml.actions.EtatDecrementer;
import org.linotte.moteur.xml.actions.EtatDemander;
import org.linotte.moteur.xml.actions.EtatDeplacerDe;
import org.linotte.moteur.xml.actions.EtatDeplacerVers;
import org.linotte.moteur.xml.actions.EtatDeplacerVersBas;
import org.linotte.moteur.xml.actions.EtatDeplacerVersDroite;
import org.linotte.moteur.xml.actions.EtatDeplacerVersGauche;
import org.linotte.moteur.xml.actions.EtatDeplacerVersHaut;
import org.linotte.moteur.xml.actions.EtatDiviser;
import org.linotte.moteur.xml.actions.EtatEffacer;
import org.linotte.moteur.xml.actions.EtatEffacerLaToile;
import org.linotte.moteur.xml.actions.EtatEffacerLeTableau;
import org.linotte.moteur.xml.actions.EtatEspece;
import org.linotte.moteur.xml.actions.EtatEssayer;
import org.linotte.moteur.xml.actions.EtatEvaluer;
import org.linotte.moteur.xml.actions.EtatEvoquer;
import org.linotte.moteur.xml.actions.EtatExtraire;
import org.linotte.moteur.xml.actions.EtatFaire;
import org.linotte.moteur.xml.actions.EtatFaireReagir;
import org.linotte.moteur.xml.actions.EtatFermer;
import org.linotte.moteur.xml.actions.EtatFermerTube;
import org.linotte.moteur.xml.actions.EtatFusionner;
import org.linotte.moteur.xml.actions.EtatImportLivre;
import org.linotte.moteur.xml.actions.EtatImportation;
import org.linotte.moteur.xml.actions.EtatIncrementer;
import org.linotte.moteur.xml.actions.EtatInserer;
import org.linotte.moteur.xml.actions.EtatInterrompre;
import org.linotte.moteur.xml.actions.EtatInverser;
import org.linotte.moteur.xml.actions.EtatLire;
import org.linotte.moteur.xml.actions.EtatLivre;
import org.linotte.moteur.xml.actions.EtatMelanger;
import org.linotte.moteur.xml.actions.EtatMesurer;
import org.linotte.moteur.xml.actions.EtatModifier;
import org.linotte.moteur.xml.actions.EtatMultiplier;
import org.linotte.moteur.xml.actions.EtatObserver;
import org.linotte.moteur.xml.actions.EtatOter;
import org.linotte.moteur.xml.actions.EtatParagraphe;
import org.linotte.moteur.xml.actions.EtatParcourir;
import org.linotte.moteur.xml.actions.EtatParcourirDe;
import org.linotte.moteur.xml.actions.EtatPause;
import org.linotte.moteur.xml.actions.EtatPeindre;
import org.linotte.moteur.xml.actions.EtatPhotographier;
import org.linotte.moteur.xml.actions.EtatPlusFaireReagir;
import org.linotte.moteur.xml.actions.EtatProjeter;
import org.linotte.moteur.xml.actions.EtatProposer;
import org.linotte.moteur.xml.actions.EtatQuestionner;
import org.linotte.moteur.xml.actions.EtatRecharger;
import org.linotte.moteur.xml.actions.EtatRetourner;
import org.linotte.moteur.xml.actions.EtatRevenir;
import org.linotte.moteur.xml.actions.EtatRole;
import org.linotte.moteur.xml.actions.EtatSouffleur;
import org.linotte.moteur.xml.actions.EtatSoustraire;
import org.linotte.moteur.xml.actions.EtatSoustraireSimple;
import org.linotte.moteur.xml.actions.EtatStimuler;
import org.linotte.moteur.xml.actions.EtatStopper;
import org.linotte.moteur.xml.actions.EtatStructureDebut;
import org.linotte.moteur.xml.actions.EtatStructureGlobale;
import org.linotte.moteur.xml.actions.EtatTemporiser;
import org.linotte.moteur.xml.actions.EtatTestUnitaire;
import org.linotte.moteur.xml.actions.EtatTestUnitaireIn;
import org.linotte.moteur.xml.actions.EtatTestUnitaireOut;
import org.linotte.moteur.xml.actions.EtatTilde;
import org.linotte.moteur.xml.actions.EtatTournerADroite;
import org.linotte.moteur.xml.actions.EtatTournerAGauche;
import org.linotte.moteur.xml.actions.EtatTrier;
import org.linotte.moteur.xml.actions.EtatValoir;
import org.linotte.moteur.xml.actions.EtatVider;
import org.linotte.moteur.xml.alize.kernel.Action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RegistreDesActions {

	public RegistreDesActions() {
		initEtatsDuSysteme();
	}

	private Map<String, Action> map = null;

	private static List<String> erreurs = new ArrayList<String>();

	private EtatTemporiser temporiser;
	private EtatPause pause;
	private EtatAttendreSeconde attendreSeconde;
	private EtatAttendreMilliSeconde attendreMilliSeconde;
	private EtatAppeler etatAppeler;
	private EtatCondition etatCondition;
	private EtatArreter etatArreter;

	public void initEtatsDuSysteme() {

		if (map == null) {
			map = new HashMap<String, Action>();
			addAction(new EtatLivre());
			addAction(new EtatActeur());
			addAction(new EtatEspece());
			addAction(new EtatRole());
			addAction(new EtatParagraphe());
			addAction(new EtatAfficher());
			addAction(new EtatDemander());
			addAction(new EtatCopier());
			addAction(new EtatMesurer());
			addAction(new EtatAjouter());
			addAction(new EtatAction());
			addAction(new EtatEffacerLeTableau());
			addAction(new EtatQuestionner());
			addAction(etatArreter = new EtatArreter());
			addAction(new EtatPause());
			addAction(new EtatTemporiser());
			addAction(new EtatAttendreSeconde());
			addAction(new EtatAttendreMilliSeconde());
			addAction(new EtatMultiplier());
			addAction(new EtatSoustraire());
			addAction(new EtatMelanger());
			addAction(new EtatTrier());
			addAction(new EtatVider());
			addAction(new EtatLire());
			addAction(new EtatAller());
			addAction(new EtatFaire());
			addAction(new EtatRevenir());
			addAction(new EtatBoucle());
			addAction(etatCondition = new EtatCondition());
			addAction(new EtatConditionSinon());
			addAction(new EtatDiviser());
			addAction(new EtatInverser());
			addAction(new EtatExtraire());
			addAction(new EtatInserer());
			addAction(new EtatChercher());
			addAction(new EtatConcatener());
			addAction(new EtatValoir());
			addAction(new EtatProjeter());
			addAction(new EtatDeplacerVers());
			addAction(new EtatEffacerLaToile());
			addAction(new EtatEffacer());
			addAction(new EtatDeplacerDe());
			addAction(new EtatDeplacerVersGauche());
			addAction(new EtatDeplacerVersDroite());
			addAction(new EtatDeplacerVersHaut());
			addAction(new EtatDeplacerVersBas());
			addAction(new EtatPhotographier());
			addAction(new EtatStimuler());
			addAction(new EtatConvertir());
			addAction(new EtatFermer());
			addAction(new EtatParcourir());
			addAction(new EtatImportLivre());
			addAction(new EtatParcourirDe());
			addAction(new EtatOter());
			addAction(new EtatModifier());
			addAction(new EtatAvancerDe());
			addAction(new EtatTournerADroite());
			addAction(new EtatTournerAGauche());
			addAction(new EtatAppeler());
			addAction(new EtatObserver());
			addAction(new EtatFermerTube());
			addAction(new EtatCharger());
			addAction(new EtatDecharger());
			addAction(new EtatConfigurer());
			addAction(new EtatEvaluer());
			addAction(new EtatSouffleur());
			addAction(new EtatFusionner());
			addAction(new EtatFaireReagir());
			addAction(new EtatRetourner());
			addAction(new EtatEssayer());
			addAction(new EtatPeindre());
			addAction(new EtatPlusFaireReagir());
			addAction(new EtatImportation());
			addAction(new EtatAnnihiler());
			addAction(new EtatAttacher());
			addAction(new EtatEvoquer());
			addAction(new EtatDecrementer());
			addAction(new EtatIncrementer());
			addAction(new EtatTilde());
			addAction(new EtatTestUnitaireIn());
			addAction(new EtatTestUnitaireOut());
			addAction(new EtatTestUnitaire());
			addAction(new EtatCompatibilite());
			addAction(new EtatStopper());
			addAction(new EtatProposer());
			addAction(new EtatStructureGlobale());
			addAction(new EtatStructureDebut());
			addAction(new EtatRecharger());
			addAction(new EtatInterrompre());
			addAction(new EtatAjouterSimple());
			addAction(new EtatSoustraireSimple());
		}
	}

	public void addAction(Action etat) {
		if (etat instanceof EtatAttendreSeconde) {
			attendreSeconde = (EtatAttendreSeconde) etat;
		} else if (etat instanceof EtatAttendreMilliSeconde) {
			attendreMilliSeconde = (EtatAttendreMilliSeconde) etat;
		} else if (etat instanceof EtatTemporiser) {
			temporiser = (EtatTemporiser) etat;
		} else if (etat instanceof EtatPause) {
			pause = (EtatPause) etat;
		} else if (etat instanceof EtatAppeler) {
			etatAppeler = (EtatAppeler) etat;
		}
		map.put(etat.clef(), etat);
	}

	public Action retourneAction(String clef) {
		return map.get(clef);
	}

	public static void ajouterErreur(String message) {
		erreurs.add(message);
	}

	public static Iterator<String> retourneErreurs() {
		Iterator<String> i = erreurs.iterator();
		return i;
	}
	
	public static void effaceErreurs() {
		erreurs.clear();
	}


	public EtatTemporiser getTemporiser() {
		return temporiser;
	}

	public EtatPause getPause() {
		return pause;
	}

	public EtatAttendreSeconde getAttendreSeconde() {
		return attendreSeconde;
	}

	public EtatAttendreMilliSeconde getAttendreMilliSeconde() {
		return attendreMilliSeconde;
	}

	public EtatAppeler getActionAppeler() {
		return etatAppeler;
	}

	public EtatCondition getActionCondition() {
		return etatCondition;
	}

	public EtatArreter getActionArreter() {
		return etatArreter;
	}

}
