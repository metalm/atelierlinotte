package org.linotte.moteur.xml;

import org.linotte.moteur.outils.Ressources;

import java.util.Properties;

public class Version {

    private static Properties bundle;

    static {
        bundle = new Properties();//.getBundle("assets.version");
        try {
            bundle.load(Ressources.getFlux("version.properties"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final static String VERSION_RELEASE = bundle.getProperty("VERSION_RELEASE");
    private final static String VERSION_TECHNIQUE = bundle.getProperty("VERSION_TECHNIQUE");
    private final static String VERSION_TECHNIQUE_URL = bundle.getProperty("VERSION_TECHNIQUE_URL");
    private final static String URL_HOME = bundle.getProperty("URL_HOME");
    private final static String VERSION_WORK = bundle.getProperty("VERSION_WORK");
    private final static boolean BETA = Boolean.parseBoolean(bundle.getProperty("BETA"));
    private final static boolean PRO = Boolean.parseBoolean(bundle.getProperty("PRO"));
    public final static String AUTEUR = bundle.getProperty("AUTEUR");
    public final static String LICENCE = bundle.getProperty("LICENCE");
    public final static String URL_GREFFONS = bundle.getProperty("URL_GREFFONS");
    public final static String DATE = bundle.getProperty("DATE");

    public static String getVersion() {
        return (!BETA) ? VERSION_RELEASE : (VERSION_WORK);
    }

    public static String getVERSION_TECHNIQUE() {
        return VERSION_TECHNIQUE;
    }

    public static String getVERSION_TECHNIQUE_URL() {
        return VERSION_TECHNIQUE_URL;
    }

    public static String getURL() {
        return URL_HOME;
    }

}