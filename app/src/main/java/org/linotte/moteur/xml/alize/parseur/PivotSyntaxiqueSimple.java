/***********************************************************************
 * Linotte                                                             *
 * Version release date : February 14, 2013                            *
 * Author : Mounes Ronan ronan.mounes@amstrad.eu                       *
 *                                                                     *
 *     http://langagelinotte.free.fr                                   *
 *                                                                     *
 * This code is released under the GNU GPL license, version 2 or       *
 * later, for educational and non-commercial purposes only.            *
 * If any part of the code is to be included in a commercial           *
 * software, please contact us first for a clearance at                *
 *   ronan.mounes@amstrad.eu                                           *
 *                                                                     *
 *   This notice must remain intact in all copies of this code.        *
 *   This code is distributed WITHOUT ANY WARRANTY OF ANY KIND.        *
 *   The GNU GPL license can be found at :                             *
 *           http://www.gnu.org/copyleft/gpl.html                      *
 *                                                                     *
 *                 Pivot Syntaxique                                    *
 *                                                                     *
 ***********************************************************************/

package org.linotte.moteur.xml.alize.parseur;

import org.linotte.moteur.xml.actions.EtatActeur;
import org.linotte.moteur.xml.actions.EtatEspece;
import org.linotte.moteur.xml.actions.EtatImportLivre;
import org.linotte.moteur.xml.actions.EtatImportation;
import org.linotte.moteur.xml.actions.EtatParagraphe;
import org.linotte.moteur.xml.actions.EtatProposer;
import org.linotte.moteur.xml.actions.EtatStructureDebut;
import org.linotte.moteur.xml.actions.EtatStructureGlobale;
import org.linotte.moteur.xml.actions.EtatTestUnitaire;
import org.linotte.moteur.xml.actions.EtatTestUnitaireIn;
import org.linotte.moteur.xml.actions.EtatTestUnitaireOut;
import org.linotte.moteur.xml.alize.kernel.Action;
import org.linotte.moteur.xml.alize.kernel.Job;
import org.linotte.moteur.xml.alize.kernel.processus.Processus;
import org.linotte.moteur.xml.alize.kernel.processus.ProcessusFactory;
import org.linotte.moteur.xml.analyse.ItemXML;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe va vérifier si le livre a une structure cohérente.
 * 
 *  Voici les régles vérifiées :
 *  
 *  - [X] si structure sans paragraphe au début du livre, s'arréter avant le premier paragraphe
 *  
 *  Voici les optimisations effectuées :
 *  
 *  - 
 *
 */
public class PivotSyntaxiqueSimple {

	private Job jobRacine;
	private ParserContext pc;
	private List<Processus> deja = new ArrayList<Processus>();

	private enum ETAT {
		INITIALISATION, FIN_TRAITEMENT
	}

	public PivotSyntaxiqueSimple(ParserContext pc, Job jobRacine) {
		this.jobRacine = jobRacine;
		this.pc = pc;
	}

	public void verifier() throws Exception {
		verifier((Processus) jobRacine.getFirstProcessus(), ETAT.INITIALISATION);
	}

	private void verifier(Processus pere, ETAT etat) throws Exception {
		boolean verbe = false;
		Processus p = pere;
		Processus precedent = null;
		Action action = null;
		while (true) {
			precedent = p;
			p = (Processus) p.getNextProcess();
			if (p == null)
				etat = ETAT.FIN_TRAITEMENT;
			else
				action = p.getEtat();
			switch (etat) {
			case INITIALISATION:
				if (action instanceof EtatTestUnitaire || action instanceof EtatEspece || action instanceof EtatActeur
						|| action instanceof EtatProposer || action instanceof EtatImportation || action instanceof EtatTestUnitaireOut
						|| action instanceof EtatTestUnitaireIn || action instanceof EtatImportLivre || action instanceof EtatStructureGlobale
						|| action instanceof EtatStructureDebut) {
					break; // Rien à faire
				} else if (action instanceof EtatParagraphe) {
					etat = ETAT.FIN_TRAITEMENT;
					deja.add(p);
					if (verbe) {
						Processus processus = ProcessusFactory.createProcessus("fin", new ArrayList<ItemXML>(), new ArrayList(), pc.linotte.getRegistreDesEtats()
								.getActionArreter(), p.getPosition());
						precedent.setNextProcess(processus);
					}
					break;
				} else {
					// verbe ?
					verbe = true;
					break;
				}
			case FIN_TRAITEMENT:
				return;
			default:
				break;
			}
		}
	}

}