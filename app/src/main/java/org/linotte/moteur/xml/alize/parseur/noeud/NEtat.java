/***********************************************************************
 * Linotte                                                             *
 * Version release date : September 01, 2006                           *
 * Author : Mounes Ronan ronan.mounes@amstrad.eu                       *
 *                                                                     *
 *     http://langagelinotte.free.fr                                   *
 *                                                                     *
 * This code is released under the GNU GPL license, version 2 or       *
 * later, for educational and non-commercial purposes only.            *
 * If any part of the code is to be included in a commercial           *
 * software, please contact us first for a clearance at                *
 *   ronan.mounes@amstrad.eu                                           *
 *                                                                     *
 *   This notice must remain intact in all copies of this code.        *
 *   This code is distributed WITHOUT ANY WARRANTY OF ANY KIND.        *
 *   The GNU GPL license can be found at :                             *
 *           http://www.gnu.org/copyleft/gpl.html                      *
 *                                                                     *
 ***********************************************************************/

package org.linotte.moteur.xml.alize.parseur.noeud;

import org.linotte.moteur.exception.Constantes;
import org.linotte.moteur.exception.ErreurException;
import org.linotte.moteur.xml.actions.EtatFermer;
import org.linotte.moteur.xml.actions.EtatLire;
import org.linotte.moteur.xml.actions.EtatParagraphe;
import org.linotte.moteur.xml.alize.kernel.Action;
import org.linotte.moteur.xml.alize.kernel.i.ParserHandler;
import org.linotte.moteur.xml.alize.kernel.processus.Processus;
import org.linotte.moteur.xml.alize.kernel.processus.ProcessusDispatcher;
import org.linotte.moteur.xml.alize.kernel.processus.ProcessusFactory;
import org.linotte.moteur.xml.alize.parseur.ParserContext;
import org.linotte.moteur.xml.alize.parseur.ParserContext.ActeurGlobal;
import org.linotte.moteur.xml.alize.parseur.ParserContext.MODE;
import org.linotte.moteur.xml.alize.parseur.ParserContext.Prototype;
import org.linotte.moteur.xml.alize.parseur.a.Noeud;
import org.linotte.moteur.xml.analyse.GroupeItemXML;
import org.linotte.moteur.xml.analyse.ItemXML;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class NEtat extends Noeud {

	public NEtat(Node n) {
		super(n);
	}

	public NEtat(NEtat n) {
		super(n);
	}

	@Override
	public Noeud cloner() {
		return new NEtat(this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean parse(ParserContext pc) throws Exception {

		List<ItemXML> tableau = new ArrayList<ItemXML>();
		Iterator<?> i = pc.valeurs.iterator();
		while (i.hasNext()) {
			List<ItemXML> l = (List<ItemXML>) i.next();
			while (!l.isEmpty()) {
				tableau.add(l.remove(0));
			}
		}

		List<String> tableau_annotations = new ArrayList<String>();
		Iterator<?> i2 = pc.annotations.iterator();
		while (i2.hasNext()) {
			List<String> l = (List<String>) i2.next();
			while (!l.isEmpty()) {
				tableau_annotations.add(l.remove(0));
			}
		}

		// Gestion de la complétion des prototypes :

		if (pc.mode == MODE.COLORATION) {
			if (tableau_annotations.size() > 0) {
				if (tableau_annotations.contains("prototype")) {
					pc.types_prototypes.put(tableau.get(0).toString().toLowerCase(), new ArrayList<String>());
					if (tableau_annotations.contains("atributespece")) {
						int debut = 1;
						String[] tab_a = new String[tableau.size() - debut];
						int max_tab_a = tableau.size();
						for (int ia = debut; ia < max_tab_a; ia++) {
							tab_a[ia - debut] = tableau.get(ia).toString().toLowerCase();
						}
						pc.prototypes_attributs.put(tableau.get(0).toString().toLowerCase(), tab_a);
					}
				}
				if (tableau_annotations.contains("instanceprototype")) {
					// Il faut mettre dans un pile ?
					String nom = tableau.get(0).toString().toLowerCase();
					if (nom.startsWith("* "))
						nom = nom.substring(2);
					pc.prototypes.add(new Prototype(tableau.get(1).toString().toLowerCase(), nom, pc.lexer.getLastPosition()));
				}

				if (tableau_annotations.contains("nomacteur")) {
					ItemXML itxml = tableau.get(0);
					if (itxml instanceof GroupeItemXML) {
						ItemXML[] temp = ((GroupeItemXML) itxml).items;
						int bogue = 0;
						for (ItemXML t : temp) {
							String nom = t.toString().toLowerCase();
							if (tableau.size() > 1)
								pc.acteursgloblaux.add(new ActeurGlobal(tableau.get(1).toString().toLowerCase(), nom, pc.lexer.getLastPosition() + bogue));
							else
								pc.acteursgloblaux.add(new ActeurGlobal("", nom, pc.lexer.getLastPosition() + bogue));
							bogue++;
							// Linote 2.6.2 : complétion des types simples 
							pc.prototypes.add(new Prototype(getAttribut("param"), nom, pc.lexer.getLastPosition()));
						}
					} else {
						String nom = itxml.toString().toLowerCase();
						if (tableau.size() > 1)
							pc.acteursgloblaux.add(new ActeurGlobal(tableau.get(1).toString().toLowerCase(), nom, pc.lexer.getLastPosition()));
						else
							pc.acteursgloblaux.add(new ActeurGlobal("", nom, pc.lexer.getLastPosition()));
						// Linote 2.6.2 : complétion des types simples 
						pc.prototypes.add(new Prototype(getAttribut("param"), nom, pc.lexer.getLastPosition()));
					}
				}

				if (tableau_annotations.contains("héritage")) {
					pc.prototypes_heritage.put(tableau.get(0).toString().toLowerCase(), tableau.get(1).toString().toLowerCase());
				}

				if (tableau_annotations.contains("joker")) {
					//TODO A OPTIMISER //
					//URGENT
					String acteur_boucle = tableau.get(0).toString().toLowerCase();
					String type = null;
					// On recherche l'acteur :
					label: for (Set<org.linotte.moteur.xml.alize.parseur.ParserContext.Prototype> set : pc.constructionPileContexteVariables) {
						for (org.linotte.moteur.xml.alize.parseur.ParserContext.Prototype prototype : set) {
							if (prototype.nom.equals(acteur_boucle)) {
								type = prototype.type;
								break label;
							}
						}
					}
					if (type == null)
						for (ActeurGlobal ag : pc.acteursgloblaux) {
							if (ag.nom.equals(acteur_boucle)) {
								type = ag.type;
								break;
							}
						}

					if (!pc.constructionPileContexteVariables.isEmpty()) {
						pc.constructionPileContexteVariables.peek().add(new Prototype(type, "joker", pc.lexer.getLastPosition()));
					}
				}

				if (tableau_annotations.contains("nomacteurfonction")) {
					ItemXML itxml = tableau.get(0);
					ajouterActeursFonction(pc, tableau, itxml);
					String nom = itxml.toString().toLowerCase();
					// Linote 2.6.2 : complétion des types simples 
					pc.prototypes.add(new Prototype(getAttribut("param"), nom, pc.lexer.getLastPosition()));
				}
				// Paramètres simplifiés :
				if (tableau_annotations.contains("nomacteurfonctionsimple")) {
					for (int idx=1;idx< tableau.size();idx++) {
						ItemXML itxml = tableau.get(idx);
						ajouterActeursFonction(pc, tableau, itxml);					
					}
				}
			}
		}

		if (pc.mode == MODE.GENERATION_RUNTIME) {

			Action etat = pc.linotte.getRegistreDesEtats().retourneAction(getAttribut("nom"));

			if (etat != null) {

				// boolean souffleur_activer =
				// !"non".equals(node.getAttribut("souffleur"));

				boolean syntaxev2 = pc.etats.size() == 0; // "compatiblev2".equals(getAttribut("param"));

				if ((!pc.lexer.isFinDeLigne() || hack) && !syntaxev2) {
					// debug("==> C'est pas l'état de fin de ligne !");
					Processus processusPrincipal = ProcessusFactory.createProcessus(getAttribut("param"), tableau, tableau_annotations, etat,
							pc.lastPositionLigne);
					((List<Processus>) pc.etats.peek()).add(processusPrincipal);

					if (etat instanceof ParserHandler) {
						pc.awares.add(processusPrincipal);
					}
				} else {
					Iterator<?> it = pc.etats.iterator();
					List<Processus> tabetat = new ArrayList<Processus>();
					while (it.hasNext()) {
						List<Processus> temp = (List<Processus>) it.next();
						tabetat.addAll(temp);
						temp.clear();
					}
					Processus processus = ProcessusFactory.createProcessus(getAttribut("param"), tableau, tableau_annotations, etat, pc.lastPositionLigne);

					if (etat instanceof ParserHandler) {
						pc.awares.add(processus);
					}

					Processus processusPrimaire = tabetat.size() > 0 ? tabetat.get(0) : null;

					if (pc.souffleurs.size() > 0 && !"non".equals(getAttribut("souffleur")))
						processus.addSouffleurs(pc.souffleurs);

					if (processusPrimaire != null) {

						// Peut être un début de paragraphe ?
						if (etat instanceof EtatLire) {
							pc.constructionPileSousParagraphesProcessus.push(processus);
						}

						if (processusPrimaire instanceof ProcessusDispatcher)
							((ProcessusDispatcher) processusPrimaire).setProcessusSecondaire(processus);
						if (processus instanceof ProcessusDispatcher)
							((ProcessusDispatcher) processus).setProcessusPrimaire(processusPrimaire);

						processus = processusPrimaire;

						// Souffleurs ?
						if (pc.souffleur) {
							pc.souffleurs.add(processus);
						}

					} else {
						// Peut être une fin de paragraphe ?
						if (etat instanceof EtatFermer) {
							if (pc.constructionPileSousParagraphesProcessus.size() == 0) {
								throw new ErreurException(Constantes.SYNTAXE_SOUS_PARAGRAPHE);
							}
							Processus lire = pc.constructionPileSousParagraphesProcessus.pop();
							// TODO Contrôler erreur
							pc.environnement.getSousParagraphes().put(lire, processus);
						}

					}

					if (processus != null) {
						if (pc.lastProcessus == null) {
							pc.jobRacine.setFirstProcessus(processus);
							pc.lastProcessus = processus;
						} else {
							if (!(etat instanceof EtatParagraphe) || (pc.premier_paragraphe)) {
								// Si c'est le premier paragraphe mais c'est une
								// fontion d'un prototype, on n execute pas.
								if (pc.premier_paragraphe && pc.methodeFonctionnellePrototype != null && etat instanceof EtatParagraphe) {
									// Il doit être exécuté mais on ne veut pas
									// !
									// On va garder le pointeur pour y mettre la
									// premiere fonction exécutée.
									pc.processusNonFonctionPrototype = pc.lastProcessus;
								} else {
									pc.lastProcessus.setNextProcess(processus);
								}
							}
							// Même action pour le processus secondaire
							// :
							if (!(etat instanceof EtatParagraphe) && (pc.lastProcessus instanceof ProcessusDispatcher)
									&& ((ProcessusDispatcher) pc.lastProcessus).getProcessusSecondaire() != null) {
								((ProcessusDispatcher) pc.lastProcessus).getProcessusSecondaire().setNextProcess(processus);
							}
							// Par contre, si processus secondaire est
							// souffleur :
							if (!(etat instanceof EtatParagraphe) && (pc.lastProcessus instanceof ProcessusDispatcher)
									&& ((ProcessusDispatcher) pc.lastProcessus).getProcessusSecondaire() != null && pc.souffleur) {
								((ProcessusDispatcher) pc.lastProcessus).getProcessusSecondaire().setNextProcess(null);
								((ProcessusDispatcher) pc.lastProcessus).setNextProcess(null);
							}

							pc.lastProcessus = processus;
						}

					}

					if (etat instanceof EtatParagraphe) {
						if (pc.processusNonFonctionPrototype != null && pc.processusNonFonctionPrototype.getNextProcess() == null
								&& pc.methodeFonctionnellePrototype == null) {
							// C'est la premiere fonction à exécuter !
							pc.processusNonFonctionPrototype.setNextProcess(processus);
							pc.processusNonFonctionPrototype = null;
						}
						pc.premier_paragraphe = false;
						if (pc.dernierParagraphe == null)
							pc.dernierParagraphe = "§";
						// Mettre dans prototype ???
						if (pc.methodeFonctionnellePrototype != null) {
							pc.environnement.addParagraphe(pc.methodeFonctionnellePrototype, pc.dernierParagraphe, pc.lastProcessus);
							pc.methodeFonctionnellePrototype = null;
						} else
							pc.environnement.setParagraphes(pc.dernierParagraphe, pc.lastProcessus);
						pc.souffleurs.clear();
					}
				}
			}
		}
		return true;
	}

	private void ajouterActeursFonction(ParserContext pc, List<ItemXML> tableau, ItemXML itxml) {
		if (itxml instanceof GroupeItemXML) {
			ItemXML[] temp = ((GroupeItemXML) itxml).items;
			for (ItemXML t : temp) {
				ajouterActeurFonction(pc, tableau, t);
			}
		} else {
			ajouterActeurFonction(pc, tableau, itxml);
		}
	}

	private void ajouterActeurFonction(ParserContext pc, List<ItemXML> tableau, ItemXML itxml) {
		String nom = itxml.toString().toLowerCase();
		if (nom.startsWith("* "))
			nom = nom.substring(2);
		if (!pc.constructionPileContexteVariables.isEmpty()) {
			if (tableau.size() > 1)
				pc.constructionPileContexteVariables.peek().add(new Prototype(tableau.get(1).toString().toLowerCase(), nom, pc.lexer.getLastPosition()));
			else
				pc.constructionPileContexteVariables.peek().add(new Prototype("??", nom, pc.lexer.getLastPosition()));
		}
	}

}