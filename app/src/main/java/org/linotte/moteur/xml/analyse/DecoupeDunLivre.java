/***********************************************************************
 * Linotte                                                             *
 * Version release date : January 11, 2006                             *
 * Author : Mounes Ronan ronan.mounes@amstrad.eu                       *
 *                                                                     *
 *     http://langagelinotte.free.fr                                   *
 *                                                                     *
 * This code is released under the GNU GPL license, version 2 or       *
 * later, for educational and non-commercial purposes only.            *
 * If any part of the code is to be included in a commercial           *
 * software, please contact us first for a clearance at                *
 *   ronan.mounes@amstrad.eu                                           *
 *                                                                     *
 *   This notice must remain intact in all copies of this code.        *
 *   This code is distributed WITHOUT ANY WARRANTY OF ANY KIND.        *
 *   The GNU GPL license can be found at :                             *
 *           http://www.gnu.org/copyleft/gpl.html                      *
 *                                                                     *
 *                 Lexer                                               *
 *                                                                     *
 ***********************************************************************/

package org.linotte.moteur.xml.analyse;

import org.linotte.moteur.exception.FinException;
import org.linotte.moteur.xml.alize.parseur.a.Lexer;
import org.linotte.moteur.xml.exception.NouvelleLigne;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * Réécriture du lexer à faire !!!!
 * 
 * @author cpc
 *
 */
public class DecoupeDunLivre implements Lexer {

	private static final String CHAR_RETOUR = "\n";

	private static final String CHAR_ESPACE = " ";

	private static final String[] _CHAR_PARTICULIERS = { String.valueOf((char) 160), String.valueOf((char) 65279) };

	private static final List<String> CHAR_PARTICULIERS = Arrays.asList(_CHAR_PARTICULIERS);

	private static final List<String> CHAINE_PARTICULIERS = Arrays.asList("==", "<=", ">=", ">>", "<<", "<-", "::", "!=", ":=", "+=", "-=");

	private final List<String> retourLigne = new ArrayList<String>(
			Arrays.asList(",", "+", "*", "&", "-", "=", ")", "(", "/", "==", "<=", ">=", "<-", "{", "[", "]", "}", "::", "!="));

	private static final String CHAR_TAB = "\t";

	private static final String DOT = ".";

	// Valeur dynamique Linotte 2.4
	private final String pointVirgule;

	private final String pointeurOuvrant;

	private final String pointeurFermant;
	// Fin : Valeur dynamique Linotte 2.4

	private static final String CHAR_APOSTROPHE = "'";

	private static final String PARENTHESE_OUVRANTE = "(";

	private static final String PARENTHESE_FERMANTE = ")";

	private static final int BUFFER_MIN = 40000;

	private String mot = null;

	private String[] caches_Mots = new String[BUFFER_MIN]; // Mot ou reste ligne

	private int[] caches_Position = new int[BUFFER_MIN]; // Position

	private boolean[] caches_Fin = new boolean[BUFFER_MIN]; // Fin

	private int[] caches_Last = new int[BUFFER_MIN]; // derniere position

	private boolean[] caches_Debut = new boolean[BUFFER_MIN]; // Debut

	private int[] caches_Ligne = new int[BUFFER_MIN]; // derniere position

	private int tailleMax = 0;

	private int[] relation_position_mot;

	private int[] historique;

	private int position = 0;

	private int positionAvantCommentaire = 0;

	private boolean finDeLigne = false;

	private boolean debutDeLigne = true;

	private StringBuilder flux = null;

	private boolean faireExeptionFinDeLigne = false;

	private List<String> findeligne2 = null;

	private int fichierTailleMax;

	private int lastPosition = 0;

	private int ligneCourante = 0;

	private String debutchaine = null;

	public int premierMotDepose = -1;

	private Grammaire grammaire;

	private String CD = "/";
	private String CF = "/";
	private String CC = "*";

	public DecoupeDunLivre(Grammaire grammaire, StringBuilder pflux) {
		this.grammaire = grammaire;
		pointVirgule = grammaire.getLangage().getSeparateurLigne();
		pointeurOuvrant = grammaire.getLangage().getPointeurDebut();
		pointeurFermant = grammaire.getLangage().getPointeurFin();
		// Il faut supprimer ces caractères de retourLigne si les pointeurs ne sont pas { ou }
		if (!pointeurOuvrant.equals("{")) {
			retourLigne.remove("{");
			retourLigne.remove("}");
		}
		if (!pointeurOuvrant.equals("[")) {
			retourLigne.remove("[");
			retourLigne.remove("]");
		}
		fichierTailleMax = pflux.length();
		flux = pflux;
		relation_position_mot = new int[fichierTailleMax + 1];
		historique = new int[fichierTailleMax + 1];
		Arrays.fill(relation_position_mot, -1);
		findeligne2 = new ArrayList<String>();
		findeligne2.add("\n");
		findeligne2.add(" ");
		findeligne2.add("\t");
		findeligne2.add(CD);
		findeligne2.add(CF);
		findeligne2.add(pointVirgule);
		debutchaine = ItemXML.getChaineDebut();
	}

	private void addMot(String mot, int pos, boolean fin, boolean debut, int last, int ligne) {

		if (relation_position_mot[last] != -1)
			return;

		caches_Mots[tailleMax] = mot;
		caches_Position[tailleMax] = pos;
		caches_Fin[tailleMax] = fin;
		caches_Last[tailleMax] = last;
		//caches_Correction[tailleMax] = last;
		caches_Debut[tailleMax] = debut;
		caches_Ligne[tailleMax] = ligne;
		relation_position_mot[last] = tailleMax;
		historique[position] = last;
		agrandirChaine();
	}

	private String motSuivantEnCache() throws FinException {

		int position_lecture = relation_position_mot[position];

		if (position_lecture == -1)
			return null;

		mot = caches_Mots[position_lecture];
		position = caches_Position[position_lecture];
		finDeLigne = caches_Fin[position_lecture];
		lastPosition = caches_Last[position_lecture];
		debutDeLigne = caches_Debut[position_lecture];
		ligneCourante = caches_Ligne[position_lecture];

		faireExeptionFinDeLigne = finDeLigne;

		return mot;
	}

	private void agrandirChaine() {
		tailleMax++;
		if (tailleMax >= caches_Mots.length) {
			int d = caches_Mots.length, d2 = d * 2;

			String[] c1 = new String[d2];
			int[] c2 = new int[d2];
			boolean[] c3 = new boolean[d2];
			int[] c4 = new int[d2];
			boolean[] c5 = new boolean[d2];
			int[] c6 = new int[d2];

			System.arraycopy(caches_Mots, 0, c1, 0, d);
			System.arraycopy(caches_Position, 0, c2, 0, d);
			System.arraycopy(caches_Fin, 0, c3, 0, d);
			System.arraycopy(caches_Last, 0, c4, 0, d);
			System.arraycopy(caches_Debut, 0, c5, 0, d);
			System.arraycopy(caches_Ligne, 0, c6, 0, d);

			caches_Mots = c1;
			caches_Position = c2;
			caches_Fin = c3;
			caches_Last = c4;
			caches_Debut = c5;
			caches_Ligne = c6;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#motSuivant()
	 */
	@Override
	public String motSuivant() throws FinException {

		// Bogue, on ne traite pas le cas où l'on commence par des commentaires
		// !
		// Algo : on cherche le mot et ensuite le commentaire pour se plac

		verifierLigne();

		if (motSuivantEnCache() != null)
			return mot;

		boolean chaine = false;
		short parenthese = 0;
		short crochet = 0;
		int dot = 0;
		int lastDot = 0;
		lastPosition = position;
		int debutreel;
		boolean commentaireApresMot = false;
		do {
			int position_debut_commentaire = position;
			String c = null;
			debutDeLigne = finDeLigne;
			finDeLigne = false;
			while (!findeligne2.contains(c)) {

				position++;

				if (position > fichierTailleMax)
					throw new FinException();

				c = Character.toString(flux.charAt(position - 1));
				// System.out.println((int) c.charAt(0) + ":" + c);
				if (!chaine) {
					chaine = c.equals(debutchaine);
				} else if (c.equals(debutchaine))
					chaine = false;

				// Gestion des parentheses (Linotte 2.0):
				if (!chaine) {
					if (c.equals(PARENTHESE_OUVRANTE)) {
						parenthese++;
					} else if (c.equals(PARENTHESE_FERMANTE)) {
						parenthese--;
					} else if (c.equals(CD)) {
						if ((position) < fichierTailleMax) {
							String cplus = Character.toString(flux.charAt(position));
							if (CC.equals(cplus) || CD.equals(cplus)) {
								position--;
								break;
							}
						}

					}
				}

				// Gestion des parentheses (Linotte 2.0):
				if (!chaine) {
					if (c.equals(pointeurOuvrant)) {
						crochet++;
					} else if (c.equals(pointeurFermant)) {
						crochet--;
					}
				}

				lastDot = dot;
				if (!chaine && c.equals(DOT)) {
					dot++;
				} else {
					dot = 0;
				}

				if (!finDeLigne) {
					if (c.equals(CHAR_RETOUR)) {
						ligneCourante++;
					}
					finDeLigne = isNewLine(c, lastDot);
				}

				// J'ai ajouté && c.equals("-") &&
				// Pour prendre en compte la nouvelle syntaxe a -= 1 et ne pas perdre la syntaxe 1 < -10 (Linotte 2.7)
				if (!chaine && (grammaire.getFindeligne().contains(c) || c.equals("-") ) && parenthese == 0 && crochet == 0) {
					// On veut laisser passer les formes : !=, ==, <=, >=
					String t = c;
					boolean passe = true;
					if ((position + 1) < fichierTailleMax) {
						t = t + flux.charAt(position);
						if (CHAINE_PARTICULIERS.indexOf(t) > -1) {
							if (position - 1 == lastPosition)
								// a ::nombre
								position++;
							else
								// a::nombre
								position--;
							break;
						} else{
							if (c.equals("-") ) passe = false;
						}
					}
					if (passe) {
						if ((lastPosition + 1) != position && !c.equals(CHAR_APOSTROPHE))
							position--;
						break;
					}
				}

			}
			// Supprime les espaces avant pour ne pas gêner la gestion des couleurs dans l'Atelier :

			while (((position + 1) < fichierTailleMax) && (flux.charAt(lastPosition) <= ' ' && lastPosition < position)) {
				lastPosition++;
			}
			// On tronc les 3 petits points s'ils sont présents :
			mot = flux.substring(lastPosition, (lastDot == 3) ? (position - 4) : position).trim();

			if (mot.length() == 0 && lastDot == 3) {
				lastPosition = position - 1;
			}

			debutreel = lastPosition;

			// Linotte ne sait pas interpréter le point-virgule, on le supprime
			// et on le remplace par un caractère vide.
			if (!chaine && mot.endsWith(pointVirgule)) {
				mot = mot.substring(0, mot.length() - 1);
				while (mot.length() < (position - lastPosition))
					mot += " ";
				replace(lastPosition, position, mot);
				mot = mot.trim();
			}

			// Position avant commentaire;
			setPositionAvantCommentaire(position);
			if (!finDeLigne) {
				String ccourant = null;
				String cplus = null;
				String cmoins = null;
				boolean commentaire2 = false;
				boolean commentaire_faible = false;
				for (; (position + 1) <= fichierTailleMax; position++) {
					// C
					ccourant = Character.toString(flux.charAt(position));
					// C+
					if ((position + 1) < fichierTailleMax)
						cplus = Character.toString(flux.charAt(position + 1));
					else
						cplus = null;
					// C-
					if (position > 0)
						cmoins = Character.toString(flux.charAt(position - 1));
					else
						cmoins = null;
					if (ccourant.equals(CD) && CC.equals(cplus)) {
						commentaire2 = true;
						position_debut_commentaire = position;
					} else if (!commentaire2 && ccourant.equals(CD) && CD.equals(cplus)) {
						// Forme des commentaires : // mon message
						commentaire2 = true;
						commentaire_faible = true;
						position_debut_commentaire = position;
					} else if (!commentaire_faible && ccourant.equals(CF) && CC.equals(cmoins)) {
						commentaire2 = false;
						//position++;
						setFinCommentaire(position_debut_commentaire, position);
						lastPosition = position + 1;
						if (mot.length() > 0) {
							commentaireApresMot = true;
						}
					} else if (commentaire_faible && isNewLine(ccourant, lastDot)) {
						commentaire2 = false;
						commentaire_faible = false;
						//position--;
						setFinCommentaire(position_debut_commentaire, position);
						lastPosition = position + 1;
						finDeLigne = true;
						if (mot.length() == 0) {
							commentaireApresMot = true;
						}

					} else if (!commentaire2) {
						if (ccourant.equals(CHAR_RETOUR) && retourLigne.contains(mot)) {
							//finDeLigne = true;
							lastPosition = position + 1;
							// Il faut supprimer les caractères vides :
							// Bogue : https://code.google.com/p/langagelinotte/issues/detail?id=132
							position++;
							String bogue = Character.toString(flux.charAt(position));
							while (((position + 1) < fichierTailleMax) && (bogue.equals(CHAR_ESPACE) || Character.isWhitespace(bogue.charAt(0))
									|| CHAR_PARTICULIERS.contains(bogue) || bogue.equals(CHAR_TAB))) {
								position++;
								bogue = Character.toString(flux.charAt(position));
							}
							break;
						} else if (isNewLine(ccourant, lastDot)) {
							finDeLigne = true;
							break;
						} else if (!(ccourant.equals(CHAR_ESPACE) || Character.isWhitespace(ccourant.charAt(0)) || CHAR_PARTICULIERS.contains(ccourant)
								|| ccourant.equals(CHAR_TAB))) {
							break;
						}
					}
					if (ccourant.equals(CHAR_RETOUR)) {
						ligneCourante++;
					}
				}
			}

			if (finDeLigne) {
				String ccourant = null;
				String cplus = null;
				String cmoins = null;
				boolean commentaire2 = false;
				boolean commentaire_faible = false;
				try {
					for (; position <= fichierTailleMax; position++) {
						ccourant = Character.toString(flux.charAt(position));
						// C+
						if ((position + 1) < fichierTailleMax)
							cplus = Character.toString(flux.charAt(position + 1));
						else
							cplus = null;
						// C-
						if (position > 0)
							cmoins = Character.toString(flux.charAt(position - 1));
						else
							cmoins = null;
						if (ccourant.equals(CD) && CC.equals(cplus)) {
							commentaire2 = true;
							position_debut_commentaire = position;
						} else if (!commentaire2 && ccourant.equals(CD) && CD.equals(cplus)) {
							// Forme des commentaires : // mon message
							commentaire2 = true;
							commentaire_faible = true;
							position_debut_commentaire = position;
						} else if (!commentaire_faible && ccourant.equals(CF) && CC.equals(cmoins)) {
							commentaire2 = false;
							setFinCommentaire(position_debut_commentaire, position);
							lastPosition = position + 1;
							if (mot.length() == 0) {
								commentaireApresMot = true;
							}
						} else if (commentaire_faible && isNewLine(ccourant, lastDot)) {
							commentaire2 = false;
							commentaire_faible = false;
							//position--;
							setFinCommentaire(position_debut_commentaire, position);
							lastPosition = position + 1;
							finDeLigne = true;
							if (mot.length() == 0) {
								commentaireApresMot = true;
							}
						} else if (!commentaire2) {
							if (!(ccourant.equals(CHAR_ESPACE) || Character.isWhitespace(ccourant.charAt(0)) || CHAR_PARTICULIERS.contains(ccourant)
									|| ccourant.equals(CHAR_TAB) || isNewLine(ccourant, lastDot))) {
								break;
							}
						}
						if (ccourant.equals(CHAR_RETOUR)) {
							ligneCourante++;
						}
					}
				} catch (Exception e) {
				}
			}

		} while (mot.length() == 0 || chaine || parenthese > 0 || crochet > 0);

		int position_debut_mot = commentaireApresMot ? position : position;
		int positin_debutreel = (premierMotDepose == -1) ? 0 : debutreel;

		addMot(mot, position_debut_mot, finDeLigne, debutDeLigne, positin_debutreel, ligneCourante);
		if (premierMotDepose == -1) {
			premierMotDepose = debutreel;
		}

		if (commentaireApresMot) {
			// Patch à corriger :
			// 31	;44		;<47>;	false   Le lien entre le "31" et "," est le lastPosition 
			// ,		;<69>	;87;	false   Mais il est différent... 47 !=69 donc ça plante !
			// 6		;87		;88;	false
		}
		//System.out.println(mot + ";debutreel=" + debutreel + ";commentaireApresMot=" + (commentaireApresMot ? positionAvantCommentaire : position)
		//		+ ";finDeLigne=" + finDeLigne + ";debutDeLigne=" + debutDeLigne + ";ligneCourante=" + ligneCourante);
		faireExeptionFinDeLigne = finDeLigne;
		return mot;
	}

	private void setFinCommentaire(int debut, int fin) {

	}

	private void verifierLigne() throws NouvelleLigne {
		if (faireExeptionFinDeLigne) {
			faireExeptionFinDeLigne = false;
			throw new NouvelleLigne();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#resteLigne()
	 */
	@Override
	public String resteLigne() throws FinException {

		verifierLigne();

		StringBuilder ligne = new StringBuilder();
		try {
			while (true) {
				ligne.append(motSuivant()).append(CHAR_ESPACE);
			}
		} catch (FinException e) {
		}

		mot = ligne.toString().trim();
		faireExeptionFinDeLigne = true;

		return mot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#getMot()
	 */
	@Override
	public String getMot() {
		return mot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#getPosition()
	 */
	@Override
	public int getPosition() {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#setPosition(int)
	 */
	@Override
	public void setPosition(int pos) throws FinException {
		if (pos >= historique.length)
			throw new FinException();

		this.position = historique[pos];
		// Bogue si la position est le début du livre
		if (pos != 0)
			try {
				setFaireExeptionFinDeLigne(false);
				motSuivant();
			} catch (FinException e) {
			}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#isFinDeLigne()
	 */
	@Override
	public boolean isFinDeLigne() {
		return finDeLigne;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#getLastPosition()
	 */
	@Override
	public int getLastPosition() {
		if (lastPosition == 0)
			return premierMotDepose;
		else
			return lastPosition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#retour()
	 */
	@Override
	public void retour() {
		int avant = historique[position];
		position = historique[avant];
		setFaireExeptionFinDeLigne(false);
		try {
			motSuivant();
		} catch (FinException e) {
		}
	}

	/**
	 * Vérifier s'il y a le ...\n ou le ; avant
	 * 
	 * @param c
	 * @param dot
	 * @return
	 */
	private boolean isNewLine(String c, int dot) {
		return dot != 3 && (c.equals(CHAR_RETOUR) || c.equals(pointVirgule));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#setFaireExeptionFinDeLigne(boolean)
	 */
	@Override
	public void setFaireExeptionFinDeLigne(boolean faireExeptionFinDeLigne) {
		this.faireExeptionFinDeLigne = faireExeptionFinDeLigne;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#subString(int, int)
	 */
	@Override
	public String subString(int start, int end) {
		return flux.substring(start, end);
	}

	private void replace(int start, int end, String newWord) {
		flux = flux.replace(start, end, newWord);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.linotte.moteur.xml.Lexer#getLigneCourante()
	 */
	@Override
	public int getLigneCourante() {
		return ligneCourante;
	}

	public void setPositionAvantCommentaire(int positionAvantCommentaire) {
		this.positionAvantCommentaire = positionAvantCommentaire;
	}

	@Override
	public int getPositionAvantCommentaire() {
		return positionAvantCommentaire;
	}

	@Override
	public boolean isFaireExeptionFinDeLigne() {
		return faireExeptionFinDeLigne;
	}

}
